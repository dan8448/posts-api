import EventEmitter from 'events'
import { appDataSource } from '../dbConnection'
import { dbEmitter } from './dbEmitter'

let postCount: number
let postUploadDurationSum: number
const postEmitter = new EventEmitter()

dbEmitter.on('up', async () => {
  const postUploadDurationData: [{ sum: number, count: string }] = await appDataSource.query(
    'SELECT SUM("postUploadProcessDuration"), COUNT("postUploadProcessDuration") \
           FROM post \
          WHERE "postUploadProcessDuration" IS NOT NULL'
  )
  postUploadDurationSum = postUploadDurationData[0].sum || 0
  postCount = postUploadDurationData[0].count ? parseInt(postUploadDurationData[0].count) : 0
  postEmitter.emit('finished initial run')
})

postEmitter.on('post', (duration: number) => {
  postCount += 1
  postUploadDurationSum += duration
})

const getPostCount = () => postCount

const getAvarageUploadDuration = () => Math.round(postCount ? postUploadDurationSum / postCount : 0)

export { postEmitter, getPostCount, getAvarageUploadDuration }
