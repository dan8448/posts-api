import EventEmitter from 'events'
import { dbEmitter, getPostCount, postEmitter } from '.'
import { appDataSource } from '../dbConnection'

const getPostEmitter = new EventEmitter()
let postsRetrievingDurationSum: number
let postsRetrievingRequestsAmount: number

dbEmitter.on('up', async () => {
  const postsRetrievingData: [{ sum: number, count: string }] = await appDataSource.query(
    'SELECT SUM("postsRetrivingDuration"), COUNT("postsRetrivingDuration") \
           FROM get_post_request'
  )
  postsRetrievingDurationSum = postsRetrievingData[0].sum || 0
  postsRetrievingRequestsAmount = postsRetrievingData[0].count ? parseInt(postsRetrievingData[0].count) : 0
})

getPostEmitter.on('gotPosts', async (duration: number) => {
  postsRetrievingDurationSum += duration
  postsRetrievingRequestsAmount += 1
})

const getAvaragePostsRetrievingDuration = () => Math.round(postsRetrievingRequestsAmount ? postsRetrievingDurationSum / postsRetrievingRequestsAmount : 0)

export { getPostEmitter, getAvaragePostsRetrievingDuration }
