import EventEmitter from 'events'

const dbEmitter = new EventEmitter()

export { dbEmitter }
