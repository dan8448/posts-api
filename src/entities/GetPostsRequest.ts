import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, UpdateDateColumn, CreateDateColumn } from 'typeorm'

@Entity()
class GetPostRequest extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  startPost: number

  @Column()
  limit: number

  @Column({ nullable: true, type: 'real' })
  postsRetrivingDuration: number

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}

export { GetPostRequest }
