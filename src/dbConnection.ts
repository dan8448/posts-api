import { DataSource } from 'typeorm'
import dotenv from 'dotenv'
import { dbEmitter } from './emitters/dbEmitter'

dotenv.config()

const appDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: parseInt(process.env.DB_PORT) || 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [__dirname + '/entities/**/*.ts'],
  synchronize: true
})

appDataSource.initialize()
  .then(() => {
    console.log('connected to DB successfuly')
    dbEmitter.emit('up')
  })

export { appDataSource }
