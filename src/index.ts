import { appDataSource } from './dbConnection'
import bodyParser from 'body-parser'
import express from 'express'
import dotenv from 'dotenv'
import { getPosts, getRequestsRunTime, getTopCreators, getPostsCount, createPost } from './routers'

dotenv.config()

const app = express()
const port = process.env.API_PORT || 3000

app.use(bodyParser.json())
app.use([createPost, getPosts, getPostsCount, getTopCreators, getRequestsRunTime])

app.listen(port, () => {
  console.log(`App is up and running on port ${port}`)
})
