import express, { Request, response } from 'express'
import { Post } from '../entities/Post'
import { appDataSource } from '../dbConnection'
import { getAvarageUploadDuration } from '../emitters/postEmitter'
import { getAvaragePostsRetrievingDuration } from '../emitters'

const getTopCreators = express.Router()
const getRequestsRunTime = express.Router()

getTopCreators.get('/statistics/topcreators', async (req, res) => {
  try {
    const topCreators = await appDataSource.getRepository(Post)
      .query(
        'SELECT creator, COUNT(creator) as posts_amount \
            FROM post \
            GROUP BY creator \
            ORDER BY posts_amount desc \
            LIMIT 10'
      )
    res.send(topCreators)
      .status(200)
  } catch (e) {
    res.send('Could not retrive data')
      .status(500)
  }
})

getRequestsRunTime.get('/statistics/runtime', async (req, res) => {
  try {
    const avarageUploadDuration = getAvarageUploadDuration()
    const avaragePostsRetrievingDuration = getAvaragePostsRetrievingDuration()

    res.send({ avaragePostsRetrievingDuration, avarageUploadDuration })
  } catch (e) {
    res.send('Could not retrieve data')
      .status(500)
  }
})

export { getTopCreators, getRequestsRunTime }
