import express, { Request } from 'express'
import { Post, GetPostRequest } from '../entities'
import { appDataSource } from '../dbConnection'
import { getPostEmitter } from '../emitters'

const getPosts = express.Router()

type postsRequest = Request<{}, {}, {}, {startPost: number, limit: number}>

getPosts.get('/posts', async (req: postsRequest, res) => {
  const startTime = process.hrtime()
  let runTime: [number, number]

  try {
    const startPost: number = req.query.startPost
    const limit: number = req.query.limit
    const posts = await appDataSource.manager.find(Post, {
      skip: startPost,
      take: limit,
      order: {
        created_at: 'DESC'
      }
    })
    res.send(posts)
    runTime = process.hrtime(startTime)
    const getPostRequest = new GetPostRequest()
    getPostRequest.startPost = startPost
    getPostRequest.limit = limit
    getPostRequest.postsRetrivingDuration = Math.round((runTime[0] * (10 ** 9) + runTime[1]) / (10 ** 3))

    appDataSource.manager.save(getPostRequest)
    getPostEmitter.emit('gotPosts', getPostRequest.postsRetrivingDuration)
  } catch (e) {
    res.send('Could not retrieve posts')
      .status(500)
  }
})

export { getPosts }
