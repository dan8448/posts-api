import express from 'express'
import { Post } from '../entities/Post'
import { appDataSource } from '../dbConnection'
import { postEmitter } from '../emitters/postEmitter'

const createPost = express.Router()

createPost.post('/posts', async (req, res) => {
  const startTime = process.hrtime()
  const post = new Post()
  let runTime: [number, number]

  post.creator = req.body.creator
  post.postBody = req.body.postBody
  post.title = req.body.title

  try {
    const savedPost = await appDataSource.manager.save(post)
    res.send('post saved!')
      .status(200)
    runTime = process.hrtime(startTime)
    post.postUploadProcessDuration = Math.round((runTime[0] * (10 ** 9) + runTime[1]) / (10 ** 3))
    postEmitter.emit('post', post.postUploadProcessDuration)
  } catch (e) {
    res.send('post could not be saved')
      .status(500)
  }

  try {
    appDataSource.manager.save(post)
  } catch (e) {
    console.log(e)
  }
})

export { createPost }
