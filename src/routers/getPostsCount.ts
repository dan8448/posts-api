import express, { Request } from 'express'
import { Post } from '../entities/Post'
import { appDataSource } from '../dbConnection'
import { getPostCount } from '../emitters'

const getPostsCount = express.Router()

getPostsCount.get('/postnumber', async (req, res) => {
  try {
    let postCount = getPostCount()
    if (typeof postCount !== 'number') {
      postCount = await appDataSource.manager.count(Post)
    }
    res.send({ 'post count': postCount })
      .status(200)
  } catch (e) {
    res.send('Could not retrieve post count')
      .status(500)
  }
})

export { getPostsCount }
